<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="banks">
    <html>
      <body>
        <h1>Banks</h1>
        <table border="1" bgcolor="#c6ffb3">
          <tr bgcolor="#59b300">
            <th>Name</th>
            <th>Country</th>
            <th>Type</th>
            <th>Depositor Name</th>
            <th>Depositor Address</th>
            <th>Account ID</th>
            <th>Amount</th>
            <th>Profitability</th>
            <th>Time</th>
          </tr>
          <xsl:for-each select="bank">
            <tr>
              <td><xsl:value-of select="name"/></td>
              <td><xsl:value-of select="country"/></td>
              <td><xsl:value-of select="type"/></td>
              <td><xsl:value-of select="concat(depositor/name, ' ', depositor/surname)"/></td>
              <td><xsl:value-of select="concat(depositor/address/city, ', ', depositor/address/street, ', ', depositor/address/number)"/></td>
              <td><xsl:value-of select="accountID"/></td>
              <td><xsl:value-of select="amountOnDeposit"/></td>
              <td><xsl:value-of select="profitability"/></td>
              <td><xsl:value-of select="timeConstraints"/></td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>