package com.holiash.bankcomparator;

import com.holiash.model.Bank;
import java.util.Comparator;

public class BankComparator implements Comparator<Bank> {

  @Override
  public int compare(Bank o1, Bank o2) {
    return o1.getName().compareTo(o2.getName());
  }
}
