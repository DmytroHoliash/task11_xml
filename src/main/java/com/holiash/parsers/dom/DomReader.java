package com.holiash.parsers.dom;

import com.holiash.model.Address;
import com.holiash.model.Bank;
import com.holiash.model.Depositor;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DomReader {

  private List<Bank> banks = new ArrayList<>();

  public List<Bank> readXml(Document document) {
    NodeList bankElements = document.getDocumentElement().getElementsByTagName("bank");
    for (int i = 0; i < bankElements.getLength(); i++) {
      Node bank = bankElements.item(i);
      if (bank.getNodeType() == Node.ELEMENT_NODE) {
        Element bankElement = (Element) bank;
        String name = bankElement.getElementsByTagName("name").item(0).getTextContent();
        String country = bankElement.getElementsByTagName("country").item(0).getTextContent();
        String type = bankElement.getElementsByTagName("type").item(0).getTextContent();
        Element depositorElement = (Element) bankElement.getElementsByTagName("depositor").item(0);
        Depositor depositor = getDepositor(depositorElement);
        int accountID = Integer
          .parseInt(bankElement.getElementsByTagName("accountID").item(0).getTextContent());
        int amountOnDeposit = Integer
          .parseInt(bankElement.getElementsByTagName("amountOnDeposit").item(0).getTextContent());
        double profitability = Double
          .parseDouble(bankElement.getElementsByTagName("profitability").item(0).getTextContent());
        int time = Integer
          .parseInt(bankElement.getElementsByTagName("timeConstraints").item(0).getTextContent());
        banks.add(
          new Bank(name, country, type, depositor, accountID, amountOnDeposit, profitability,
            time));
      }
    }
    return banks;
  }

  private Depositor getDepositor(Element depositorElement) {
    String depositorName = depositorElement.getElementsByTagName("name").item(0).getTextContent();
    String depositorSurname = depositorElement.getElementsByTagName("surname").item(0)
      .getTextContent();
    Element depositorAddress = (Element) depositorElement.getElementsByTagName("address").item(0);
    Address address = getAddress(depositorAddress);
    return new Depositor(depositorName, depositorSurname, address);
  }

  private Address getAddress(Element depositorAddress) {
    String city = depositorAddress.getElementsByTagName("city").item(0).getTextContent();
    String street = depositorAddress.getElementsByTagName("street").item(0).getTextContent();
    String streetNumber = depositorAddress.getElementsByTagName("number").item(0)
      .getTextContent();
    return new Address(city, street, streetNumber);
  }
}
