package com.holiash.parsers.dom;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class DomDocumentCreator {

  private DocumentBuilderFactory factory;
  private DocumentBuilder builder;
  private Document document;

  public DomDocumentCreator(File xml) {
    factory = DocumentBuilderFactory.newInstance();
    try {
      builder = factory.newDocumentBuilder();
      document = builder.parse(xml);
    } catch (ParserConfigurationException | IOException | SAXException e) {
      e.printStackTrace();
    }
  }

  public Document getDocument() {
    return document;
  }
}
