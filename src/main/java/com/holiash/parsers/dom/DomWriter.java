package com.holiash.parsers.dom;

import com.holiash.model.Bank;
import com.holiash.model.Depositor;
import java.io.File;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DomWriter {

  public void write(List<Bank> banks, File file) {
    Document doc = null;
    try {
      doc = createDocument();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
    if (doc != null) {
      Element rootElement = doc.createElement("banks");
      doc.appendChild(rootElement);
      for (Bank b : banks) {
        Element bank = doc.createElement("bank");
        rootElement.appendChild(bank);
        Element name = doc.createElement("name");
        name.appendChild(doc.createTextNode(b.getName()));
        bank.appendChild(name);
        Element country = doc.createElement("country");
        country.appendChild(doc.createTextNode(b.getCountry()));
        bank.appendChild(name);
        Element type = doc.createElement("country");
        type.appendChild(doc.createTextNode(b.getType()));
        bank.appendChild(name);
        createDepositor(doc, b, bank);
        Element accountID = doc.createElement("accountID");
        accountID.appendChild(doc.createTextNode(String.valueOf(b.getAccountID())));
        bank.appendChild(accountID);
        Element amountOnDeposit = doc.createElement("amountOnDeposit");
        amountOnDeposit.appendChild(doc.createTextNode(String.valueOf(b.getAmountOnDeposit())));
        bank.appendChild(amountOnDeposit);
        Element profitability = doc.createElement("profitability");
        profitability.appendChild(doc.createTextNode(String.valueOf(b.getProfitability())));
        bank.appendChild(profitability);
        Element timeConstraints = doc.createElement("timeConstraints");
        timeConstraints.appendChild(doc.createTextNode(String.valueOf(b.getTimeConstraint())));
        bank.appendChild(timeConstraints);
      }
      outputToFile(doc, file);
    }
  }

  private void outputToFile(Document doc, File file) {
    try {
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      DOMSource source = new DOMSource(doc);
      StreamResult result = new StreamResult(file);
      transformer.transform(source, result);
    } catch (TransformerException e) {
      e.printStackTrace();
    }
  }

  private void createDepositor(Document doc, Bank b, Element parent) {
    Element depositor = doc.createElement("depositor");
    Element name = doc.createElement("name");
    name.appendChild(doc.createTextNode(b.getDepositor().getName()));
    depositor.appendChild(name);
    Element surname = doc.createElement("surname");
    surname.appendChild(doc.createTextNode(b.getDepositor().getSurname()));
    depositor.appendChild(surname);
    createAddress(doc, b.getDepositor(), depositor);
    parent.appendChild(depositor);
  }

  private void createAddress(Document doc, Depositor depositor, Element parent) {
    Element address = doc.createElement("address");
    Element city = doc.createElement("city");
    city.appendChild(doc.createTextNode(depositor.getAddress().getCity()));
    address.appendChild(city);
    Element street = doc.createElement("street");
    street.appendChild(doc.createTextNode(depositor.getAddress().getStreet()));
    address.appendChild(street);
    Element streetNum = doc.createElement("number");
    streetNum.appendChild(doc.createTextNode(depositor.getAddress().getStreetNumber()));
    address.appendChild(streetNum);
    parent.appendChild(address);
  }

  private Document createDocument() throws ParserConfigurationException {
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    return dBuilder.newDocument();
  }
}
