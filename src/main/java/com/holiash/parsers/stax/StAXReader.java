package com.holiash.parsers.stax;

import com.holiash.model.Address;
import com.holiash.model.Bank;
import com.holiash.model.Depositor;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import java.util.List;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StAXReader {

  private List<Bank> banks = new ArrayList<>();
  private Bank bank;
  private Depositor depositor;
  private Address address;
  private boolean bName = false;
  private boolean bCountry = false;
  private boolean bType = false;
  private boolean bDepositor = false;
  private boolean bDepositorName = false;
  private boolean bDepositorSurname = false;
  private boolean bCity = false;
  private boolean bStreet = false;
  private boolean bStreetNumber = false;
  private boolean bAccountId = false;
  private boolean bAmount = false;
  private boolean bProfitability = false;
  private boolean bTime = false;


  private XMLEventReader createEvent(String xml) {
    XMLInputFactory factory = XMLInputFactory.newInstance();
    XMLEventReader eventReader = null;
    try {
      eventReader = factory.createXMLEventReader(new FileReader(xml));
    } catch (XMLStreamException | FileNotFoundException e) {
      e.printStackTrace();
    }
    return eventReader;
  }

  public List<Bank> parseBanks(String xmlPath) throws XMLStreamException {
    XMLEventReader eventReader = createEvent(xmlPath);
    if (eventReader != null) {
      while (eventReader.hasNext()) {
        XMLEvent event = eventReader.nextEvent();
        String qName;
        switch (event.getEventType()) {
          case XMLStreamConstants.START_ELEMENT:
            StartElement startElement = event.asStartElement();
            qName = startElement.getName().getLocalPart();
            if (qName.equalsIgnoreCase("bank")) {
              bank = new Bank();
            } else if (qName.equalsIgnoreCase("name")) {
              if (bDepositor) {
                bDepositorName = true;
              } else {
                bName = true;
              }
            } else if (qName.equalsIgnoreCase("country")) {
              bCountry = true;
            } else if (qName.equalsIgnoreCase("type")) {
              bType = true;
            } else if (qName.equalsIgnoreCase("depositor")) {
              bDepositor = true;
              depositor = new Depositor();
            } else if (qName.equalsIgnoreCase("surname")) {
              bDepositorSurname = true;
            } else if (qName.equalsIgnoreCase("address")) {
              address = new Address();
            } else if (qName.equalsIgnoreCase("city")) {
              bCity = true;
            } else if (qName.equalsIgnoreCase("street")) {
              bStreet = true;
            } else if (qName.equalsIgnoreCase("number")) {
              bStreetNumber = true;
            } else if (qName.equalsIgnoreCase("accountID")) {
              bAccountId = true;
            } else if (qName.equalsIgnoreCase("amountOnDeposit")) {
              bAmount = true;
            } else if (qName.equalsIgnoreCase("profitability")) {
              bProfitability = true;
            } else if (qName.equalsIgnoreCase("timeConstraints")) {
              bTime = true;
            }
            break;
          case XMLStreamConstants.CHARACTERS:
            Characters characters = event.asCharacters();
            String res = characters.getData();
            if (bName) {
              bank.setName(res);
              bName = false;
            } else if (bCountry) {
              bank.setCountry(res);
              bCountry = false;
            } else if (bType) {
              bank.setType(res);
              bType = false;
            } else if (bDepositorName) {
              depositor.setName(res);
              bDepositorName = false;
            } else if (bDepositorSurname) {
              depositor.setSurname(res);
              bDepositorSurname = false;
            } else if (bCity) {
              address.setCity(res);
              bCity = false;
            } else if (bStreet) {
              address.setStreet(res);
              bStreet = false;
            } else if (bStreetNumber) {
              address.setStreetNumber(res);
              bStreetNumber = false;
            } else if (bAccountId) {
              bank.setAccountID(Integer.parseInt(res));
              bAccountId = false;
            } else if (bAmount) {
              bank.setAmountOnDeposit(Integer.parseInt(res));
              bAmount = false;
            } else if (bProfitability) {
              bank.setProfitability(Double.parseDouble(res));
              bProfitability = false;
            } else if (bTime) {
              bank.setTimeConstraint(Integer.parseInt(res));
              bTime = false;
            }
            break;
          case XMLStreamConstants.END_ELEMENT:
            EndElement endElement = event.asEndElement();
            qName = endElement.getName().getLocalPart();
            if (qName.equalsIgnoreCase("address")) {
              depositor.setAddress(address);
              address = null;
            } else if (qName.equalsIgnoreCase("depositor")) {
              bank.setDepositor(depositor);
              depositor = null;
              bDepositor = false;
            } else if (qName.equalsIgnoreCase("bank")) {
              banks.add(bank);
              bank = new Bank();
            }
            break;
        }
      }
    }
    return banks;
  }
}
