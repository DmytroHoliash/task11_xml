package com.holiash.parsers.sax;

import com.holiash.model.Bank;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class XMLParser {

  public List<Bank> parseBanks(File xml) {
    List<Bank> banks = new ArrayList<>();
    try {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      SAXParser saxParser = factory.newSAXParser();
      SAXHandler handler = new SAXHandler();
      saxParser.parse(xml, handler);
      banks = handler.getBanks();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return banks;
  }
}
