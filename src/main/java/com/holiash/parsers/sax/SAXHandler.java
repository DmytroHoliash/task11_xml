package com.holiash.parsers.sax;

import com.holiash.model.Address;
import com.holiash.model.Bank;
import com.holiash.model.Depositor;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandler extends DefaultHandler {

  private List<Bank> banks = new ArrayList<>();
  private Bank bank;
  private Depositor depositor;
  private Address address;
  private boolean bName = false;
  private boolean bCountry = false;
  private boolean bType = false;
  private boolean bDepositor = false;
  private boolean bDepositorName = false;
  private boolean bDepositorSurname = false;
  private boolean bCity = false;
  private boolean bStreet = false;
  private boolean bStreetNumber = false;
  private boolean bAccountId = false;
  private boolean bAmount = false;
  private boolean bProfitability = false;
  private boolean bTime = false;


  public List<Bank> getBanks() {
    return banks;
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
    throws SAXException {
    if (qName.equalsIgnoreCase("bank")) {
      bank = new Bank();
    } else if (qName.equalsIgnoreCase("name")) {
      if (bDepositor) {
        bDepositorName = true;
      } else {
        bName = true;
      }
    } else if (qName.equalsIgnoreCase("country")) {
      bCountry = true;
    } else if (qName.equalsIgnoreCase("type")) {
      bType = true;
    } else if (qName.equalsIgnoreCase("depositor")) {
      bDepositor = true;
      depositor = new Depositor();
    } else if (qName.equalsIgnoreCase("surname")) {
      bDepositorSurname = true;
    } else if (qName.equalsIgnoreCase("address")) {
      address = new Address();
    } else if (qName.equalsIgnoreCase("city")) {
      bCity = true;
    } else if (qName.equalsIgnoreCase("street")) {
      bStreet = true;
    } else if (qName.equalsIgnoreCase("number")) {
      bStreetNumber = true;
    } else if (qName.equalsIgnoreCase("accountID")) {
      bAccountId = true;
    } else if (qName.equalsIgnoreCase("amountOnDeposit")) {
      bAmount = true;
    } else if (qName.equalsIgnoreCase("profitability")) {
      bProfitability = true;
    } else if (qName.equalsIgnoreCase("timeConstraints")) {
      bTime = true;
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) {
    if (qName.equalsIgnoreCase("address")) {
      depositor.setAddress(address);
      address = null;
    } else if (qName.equalsIgnoreCase("depositor")) {
      bank.setDepositor(depositor);
      depositor = null;
      bDepositor = false;
    } else if (qName.equalsIgnoreCase("bank")) {
      banks.add(bank);
      bank = new Bank();
    }
  }

  @Override
  public void characters(char[] ch, int start, int length) {
    String res = new String(ch, start, length);
    if (bName) {
      bank.setName(res);
      bName = false;
    } else if (bCountry) {
      bank.setCountry(res);
      bCountry = false;
    } else if (bType) {
      bank.setType(res);
      bType = false;
    } else if (bDepositorName) {
      depositor.setName(res);
      bDepositorName = false;
    } else if (bDepositorSurname) {
      depositor.setSurname(res);
      bDepositorSurname = false;
    } else if (bCity) {
      address.setCity(res);
      bCity = false;
    } else if (bStreet) {
      address.setStreet(res);
      bStreet = false;
    } else if (bStreetNumber) {
      address.setStreetNumber(res);
      bStreetNumber = false;
    } else if (bAccountId) {
      bank.setAccountID(Integer.parseInt(res));
      bAccountId = false;
    } else if (bAmount) {
      bank.setAmountOnDeposit(Integer.parseInt(res));
      bAmount = false;
    } else if (bProfitability) {
      bank.setProfitability(Double.parseDouble(res));
      bProfitability = false;
    } else if (bTime) {
      bank.setTimeConstraint(Integer.parseInt(res));
      bTime = false;
    }
  }
}
