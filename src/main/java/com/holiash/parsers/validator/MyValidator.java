package com.holiash.parsers.validator;

import java.io.File;
import java.io.IOException;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

public class MyValidator {

  private final String XML_FILE;
  private final String XSD_FIlE;

  public MyValidator(String XML_FILE, String XSD_FIlE) {
    this.XML_FILE = XML_FILE;
    this.XSD_FIlE = XSD_FIlE;
  }

  public boolean validate() {
    File schemaFile = new File(XSD_FIlE);
    Source xmlFile = new StreamSource(new File(XML_FILE));
    SchemaFactory schemaFactory = SchemaFactory
      .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    try {
      Schema schema = schemaFactory.newSchema(schemaFile);
      Validator validator = schema.newValidator();
      validator.validate(xmlFile);
      return true;
    } catch (org.xml.sax.SAXException | IOException e) {
      return false;
    }
  }
}
