package com.holiash.model;

public class Depositor {

  private String name;
  private String surname;
  private Address address;

  public Depositor(String name, String surname, Address address) {
    this.name = name;
    this.surname = surname;
    this.address = address;
  }

  public Depositor() {
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public Address getAddress() {
    return address;
  }

  @Override
  public String toString() {
    return "Depositor{" +
      "name='" + name + '\'' +
      ", surname='" + surname + '\'' +
      ", address=" + address +
      '}';
  }
}
