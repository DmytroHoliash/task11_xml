package com.holiash.model;

public class Address {

  private String city;
  private String street;
  private String streetNumber;

  public Address(String city, String street, String streetNumber) {
    this.city = city;
    this.street = street;
    this.streetNumber = streetNumber;
  }

  public Address() {
  }

  public void setCity(String city) {
    this.city = city;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public void setStreetNumber(String streetNumber) {
    this.streetNumber = streetNumber;
  }

  public String getCity() {
    return city;
  }

  public String getStreet() {
    return street;
  }

  public String getStreetNumber() {
    return streetNumber;
  }

  @Override
  public String toString() {
    return "Address{" +
      "city='" + city + '\'' +
      ", street='" + street + '\'' +
      ", streetNumber='" + streetNumber + '\'' +
      '}';
  }
}
