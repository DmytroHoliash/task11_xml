package com.holiash.model;

public class Bank {

  private String name;
  private String country;
  private String type;
  private Depositor depositor;
  private int accountID;
  private int amountOnDeposit;
  private double profitability;
  private int timeConstraint;

  public Bank(String name, String country, String type, Depositor depositor, int accountID,
    int amountOnDeposit, double profitability, int timeConstraint) {
    this.name = name;
    this.country = country;
    this.type = type;
    this.depositor = depositor;
    this.accountID = accountID;
    this.amountOnDeposit = amountOnDeposit;
    this.profitability = profitability;
    this.timeConstraint = timeConstraint;
  }

  public Bank() {
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setDepositor(Depositor depositor) {
    this.depositor = depositor;
  }

  public void setAccountID(int accountID) {
    this.accountID = accountID;
  }

  public void setAmountOnDeposit(int amountOnDeposit) {
    this.amountOnDeposit = amountOnDeposit;
  }

  public void setProfitability(double profitability) {
    this.profitability = profitability;
  }

  public void setTimeConstraint(int timeConstraint) {
    this.timeConstraint = timeConstraint;
  }

  public String getName() {
    return name;
  }

  public String getCountry() {
    return country;
  }

  public String getType() {
    return type;
  }

  public Depositor getDepositor() {
    return depositor;
  }

  public int getAccountID() {
    return accountID;
  }

  public int getAmountOnDeposit() {
    return amountOnDeposit;
  }

  public double getProfitability() {
    return profitability;
  }

  public int getTimeConstraint() {
    return timeConstraint;
  }

  @Override
  public String toString() {
    return "Bank{" +
      "name='" + name + '\'' +
      ", country='" + country + '\'' +
      ", type='" + type + '\'' +
      ", depositor=" + depositor +
      ", accountID=" + accountID +
      ", amountOnDeposit=" + amountOnDeposit +
      ", profitability=" + profitability +
      ", timeConstraint=" + timeConstraint +
      '}';
  }
}
