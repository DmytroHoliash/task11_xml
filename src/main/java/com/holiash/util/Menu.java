package com.holiash.util;

import com.holiash.bankcomparator.BankComparator;
import com.holiash.model.Bank;
import com.holiash.parsers.dom.DomDocumentCreator;
import com.holiash.parsers.dom.DomReader;
import com.holiash.parsers.dom.DomWriter;
import com.holiash.parsers.sax.XMLParser;
import com.holiash.parsers.stax.StAXReader;
import com.holiash.parsers.validator.MyValidator;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.stream.XMLStreamException;

public class Menu {

  private Map<Integer, Command> menuCommands;
  private List<String> menu;
  private final String xml = "src\\main\\resources\\files\\banks.xml";
  private final String xsd = "src\\main\\resources\\files\\banks.xsd";
  private final MyValidator validator;

  public Menu() {
    menu = new ArrayList<>();
    menuCommands = new HashMap<>();
    validator = new MyValidator(xml, xsd);
    initMenu();
    initMenuCommand();
  }

  private void initMenu() {
    menu.add("1 - Test DOM");
    menu.add("2 - Test SAX");
    menu.add("3 - Test StAx");
    menu.add("0 - EXIT");
  }

  private void initMenuCommand() {
    menuCommands.put(1, this::testDOM);
    menuCommands.put(2, this::testSAX);
    menuCommands.put(3, this::testStAX);
  }

  private void testDOM() {
    if (validator.validate()) {
      System.out.println("Testing DOM");
      File file = new File(xml);
      File output = new File("src\\main\\resources\\files\\sortedBanks.xml");
      List<Bank> list = new DomReader().readXml(new DomDocumentCreator(file).getDocument());
      list.forEach(System.out::println);
      list.sort(new BankComparator());
      new DomWriter().write(list, output);
    } else {
      System.out.println("Invalid XML");
    }
  }

  private void testSAX() {
    if (validator.validate()) {
      System.out.println("Testing SAX");
      List<Bank> banks = new XMLParser().parseBanks(new File(xml));
      banks.forEach(System.out::println);
    } else {
      System.out.println("Invalid XML");
    }
  }

  private void testStAX() {
    if (validator.validate()) {
      System.out.println("Testing StAX");
      List<Bank> banks = new ArrayList<>();
      try {
        banks = new StAXReader().parseBanks(xml);
      } catch (XMLStreamException e) {
        e.printStackTrace();
      }
      banks.forEach(System.out::println);
    } else {
      System.out.println("Invalid XML");
    }
  }

  public void printMenu() {
    menu.forEach(System.out::println);
  }

  public void execute(int choice) {
    if (this.menuCommands.containsKey(choice)) {
      this.menuCommands.get(choice).run();
    }
  }
}
