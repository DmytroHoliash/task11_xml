package com.holiash;

import com.holiash.util.Menu;
import java.util.Scanner;

public class Application {

  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    run(new Menu());
  }

  private static void run(Menu menu) {
    int choice;
    while (true) {
      menu.printMenu();
      choice = scanner.nextInt();
      if (choice == 0) {
        break;
      }
      menu.execute(choice);
    }
  }
}
